import { Switch, Route, Link } from "react-router-dom";
import "./style.css";
import React, { useState } from 'react';
import Home from "./Home";
import Spain from "./Spain";
import Switzerland from "./Switzerland";
import Turkey from "./Turkey"; 



const App = () => {
  return (
    <div>   
    {/*Parameter*/}
      <Spain/>        
      <Turkey/>
      <Switzerland/>
      <Home/>
    </div>
  );
};

export default App;
