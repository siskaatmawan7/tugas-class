alert("Welcome To My Portofolio");
const hex = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, "A", "B", "C", "D", "E", "F"];
//index itu urutan angka 
// array tipe kumpulan data yang berurutan 


//Instructions
// 1. declare btn(id) and color(class) object by using dom syntax (getElementById and querySelector)
// 2. setup addEventListener on clicked btn object
// 3. declare the hexColor with # + 6 random number from hex array variable by using for loop function
// 4. create function getRandomNumber() by Math function to get random number of hex variable (length is considerable)
// there is a clue (by using math JS function)

var clicked=document.getElementById('btn') // mengambil element yang mempunyai id
var color=document.querySelector('.color') //titik itu class pada color 

// digunakan titik karena berdasarkan class pada html

// Membuat fungsi
getRandomNumber = () => {
    var index = Math.floor(Math.random() * hex.length);
    return hex [index];
    //return untuk mengeluarkan output dari fungsi 
    //getRandomNumber output = value didalam hex
}

//                                  menjalankan sebuah fungsi 
clicked.addEventListener('click', function() {
    var t1= getRandomNumber(); 
    var t2= getRandomNumber();
    var t3= getRandomNumber();
    var t4= getRandomNumber();
    var t5= getRandomNumber();
    var t6= getRandomNumber();

    var output= `#${t1}${t2}${t3}${t4}${t5}${t6}` // # itu tidak berubah 

    //document HTML - mengambil bagian body - mengubah CSS - properties color
    document.body.style.backgroundColor =output;
    color.innerHTML = output

});
    

// '#' untuk mendapatkan 
//string + string maka hasilnya sejajar ex : ("1" + "2") = 1 2 
//Math.floor itu fungsinya untuk menjadikan bilangan bulat 
//Math.Random berfungsi untuk mengacak 0 sampai 1 
// ${} adalah wadah untuk memanggil variable 
// color innerHTML = mengubah warna.textContent