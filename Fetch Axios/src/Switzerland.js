import React, { Component } from "react";
 
class Home extends Component {
    constructor(props){
        super(props);
        this.state =  {
            items: []
        }
    }

    componentDidMount(){
        //take random 10 result
        fetch("https://randomuser.me/api/?results=10&nat=CH")
            .then(res => res.json())
            .then(parsedJSON => parsedJSON.results.map(data => (
                {
                    //mapping data sesuai kebutuhan
                    id: `${data.id.name}`,
                    firstName: `${data.name.first}`,
                    lastName : `${data.name.last}`,
                    location: `${data.location.state}, ${data.nat}`,
                    thumbnail: `${data.picture.large}`,
                }
            )))
            .then(items => this.setState({
                items
                // isLoaded: false
            }))
            .catch(error => console.log('parsing failed', error))
    }

    render(){
        const {items } = this.state
        return(
            <div>
                <div className="boxWhite">
                    <h2>Random User</h2>
                    {
                    items.length > 0 ? items.map(item => {
                        const{id, firstName, lastName, location, thumbnail} = item
                        return(
                            <div key={id} className="bgCircle">
                                <center><img src={thumbnail} alt={firstName} className="circle"/></center><br/>
                                <div className="info">
                                    {firstName} {lastName} <br/>
                                    {location}
                                </div>
                            </div>
                         )
                        }) : null
                    }
                </div>
            </div>
        )
    }
}

export default Home;