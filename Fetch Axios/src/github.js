import React, { Component } from "react";
import axios from "axios";

 
class Home extends Component {
    constructor(props){
        super(props);
        this.state =  {
            items: []
        }
    }

    componentDidMount(){
        //take random 10 result
        axios.get("https://api.github.com/users/atma11/repos").then(response => {
            this.setState({items: response.data})
        })
    }

    render(){ //menampilkan data jsx
        const {items } = this.state
        console.log(items)
        return(
            <div>
                <h1>Github Assignment</h1> 
                <ol>
                    {items.map(data => {
                        return (<li>{data.name}</li>);
                    })} 
                </ol>
            </div>
                
        )
    }
}

export default Home;